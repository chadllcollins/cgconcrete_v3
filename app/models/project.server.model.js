const mongoose = require('mongoose');
const Schema = mongoose.Schema
 
//Note: No MongoDB support for ensuring non-duplicate nested documents
// Preventing Duplicate tasks will occur in the controller
const ProjectSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: 'Project name is required',
    default: ''
  },
  contractor: {
      type: Schema.ObjectId,
      ref: 'Contractor',
      required: 'Contractor is required'
  },
  tasks: [{
    type: Schema.ObjectId,
    ref: 'Task'
  }]
});
ProjectSchema.index({"name": 1, "contractor.name": 1}, {unique: true});
mongoose.model('Project', ProjectSchema);

/* Things to think on
  Projects cannot live without Contractor, should they even be saved separately in Mongo?
  However, to update an individual project's tasks becomes easier if they are saved as individual entities though
*/