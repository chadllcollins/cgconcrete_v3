const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TimesheetSchema = new Schema({
  lead: {
    type: String,
    required: 'Lead is required'
  },
  project: {
    type: String,
    required: 'Project is required'
  },
  contractor: {
    type: String,
    required: 'Contractor is required'
  },
  startTime: {
    type: Date, 
    required: 'Start time of the shift is required'
  },
  endTime: {
    type: Date, 
    required: 'End time of the shift is required'
  },
  weatherInfo: String,
  notes: String,
  truckNumber: {
    type: Number,
    required: 'Truck number is missing'
  },
  yardsOfConcrete: {
    type: Number,
    required: 'Amount of concrete is required'
  },
  staffed: [{
    name: {
      type: String,
      required: "Each employee requires a name"
    },
    startTime: {
    type: Date, 
    required: 'Start time for each employee is required'
    },
    endTime: {
      type: Date, 
      required: 'End time for each employee is required'
    }
  }],
  tasked: [{
    name: {
      type: String,
      required: 'Each task requires a name'
    },
    squareYards: {
      type: Number,
      required: 'Each task requires amount of yards used'
    }
  }],
  submitTime: {
    type: Date,
    default: Date.now
  }
})

//Validate Timesheet Model before saving
//Note: Is there a drawback of using 'save' middleware versus 'validate' middleware
TimesheetSchema.pre('save', function (next) {
  var valid = true;
  var message = '';
  //Staffed Validations
  //Verify no duplicate names
  var staffNamesToCount = this.staffed.
    map((emp) => {
      return {count: 1, name: emp.name}
    })
  .reduce((accumArr, curr) => {
    accumArr[curr.name] = (accumArr[curr.name] || 0) + curr.count;
    return accumArr;
  }, []);
  if(staffNamesToCount.find((staffName) => {return element.count > 1})) {
    message = 'Staff Names must be unique';
    valid = false;
  }
  //Verify proper shift times for each staffed employee
  for(var i=0; i < this.staffed.length; i++) {
    if(this.staffed[i].endTime <= this.staffed[i].startTime) {
      message = 'End time for each user must be after start time';
      valid = false;
    }
  }
  
  //Tasked Validations
  //Verify no duplicate names
  //TODO: Duplicate functionality to go into function
  var taskNamesToCount = this.tasked.
    map((task) =>{
      return {count: 1, name: task.name}
    })
  .reduce((accumArr, curr) => {
    accumArr[curr.name] = (accumArr[curr.name] || 0) + curr.count;
    return accumArr;
  }, []);
  if(taskNamesToCount.find((staffName) => {return element.count > 1})) {
    message = 'Task Names must be unique';
    valid = false;
  }
  if(this.tasked.length <= 0) {
    message = 'Must provide tasks';
    valid = false;
  }
  //Verify proper values for task yards
  for(var i=0; i < this.tasked.length; i++) {
    if ( (isNaN(this.tasked[i].squareYards)) || (this.tasked[i].squareYards <= 0)) {
      message = 'Must provide the dimensions for each task (greater than zero)';
      valid = false;
      break;
    }
  }
  
  //Verify appropriate shift times
  if(this.endTime <= this.startTime) {
    message = 'End time must be after start time'
    valid = false;
  //Verify yards Of Concrete has appropriate value greater than zero
  } else if( isNaN(this.truckNumber) || (this.truckNumber < 0)) {
    message = 'Provide appropriate truck number';
    valid = false;
  } else if (isNaN(this.yardsOfConcrete) ||  (this.yardsOfConcrete < 0)) {
    message = 'Must provide the amount of concrete (greater than zero)'
    valid = false;
  }
  if (valid) {
    next();
  }
  return next(Error(message));       
})

mongoose.model('Timesheet', TimesheetSchema);