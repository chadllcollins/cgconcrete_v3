const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmployeeSchema = new Schema({
  firstName: String,
  lastName: String,
  leadRole: Boolean
});

EmployeeSchema.index({firstName: 1, lastName: 1}, {unique: true});

EmployeeSchema.virtual('fullName').get(function() {
  return this.firstName + ' ' + this.lastName;
}).set(function(fullName) {
  const splitName = fullName.split(' ');
  this.firstName = splitName[0] || '';
  this.lastName = splitName[1] || '';
});


EmployeeSchema.set('toJSON', {
  getters: true,
  virtuals: true
});

mongoose.model('Employee', EmployeeSchema);