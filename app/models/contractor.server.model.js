const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Note: No MongoDB support for ensuring non-duplicate nested documents
//Preventing duplicate projects will occur in the controller
// No duplicate projectNames within a Contractor
const ContractorSchema = new Schema({
  name: {
    type: String,
    required: 'Contractor name is required',
    default: '',
    trim: true,
    unique: true
  }
});

mongoose.model('Contractor', ContractorSchema);