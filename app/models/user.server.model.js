var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;
//Define the UserSchema object

var UserSchema = new Schema({
  username: { //Predefined Modifier Example
    type: String,
    trim: true,
    unique: true,
    required: "Username is required"
  },
  password: {
    type: String,
    validate: [
      function(password) {
        return password && password.length >= 6;
      },
      'Password should be longer'
    ]
  },
  salt: {
    type: String
  }
});

//Pre-save method will take password and hash it before storing it
UserSchema.pre('save', function(next) {
  if (this.password) {
    this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
    this.password = this.hashPassword(this.password);
  }  
  next();
});

//Hash function
UserSchema.methods.hashPassword = function(password) {
  return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

/* 
  Instance method: Usage: instantiatedUser.authenticate('password')
  Takes password - hashes it for proper comparison
*/
UserSchema.methods.authenticate = function(password) {
  return this.password === this.hashPassword(password);
};

mongoose.model('User', UserSchema);
