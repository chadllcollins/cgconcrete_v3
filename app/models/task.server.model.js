const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Task name is required',
    unique: true,
    trim: true
  }
});

mongoose.model('Task', TaskSchema);