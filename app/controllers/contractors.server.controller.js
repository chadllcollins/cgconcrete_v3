const mongoose = require('mongoose');
const Contractor = mongoose.model('Contractor');

function getErrorMessage (err) {
  if (err.errors) {
    for (let errName in err.errors) {
      if (err.errors[errName].message) return err.errors[errName].message;
    }
  } else {
    return 'Unknown server error';
  }
};

exports.create = function(req, res) {
  var contractor = new Contractor(req.body);
  
  contractor.save(function(err) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(contractor);
    }
  });
}

exports.list = function(req, res) {
  Contractor.find().sort('name').exec(function(err, contractors) {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(contractors);
    }
  });
};

exports.contractorById = function(req, res, next, id) {
  Contractor.findById(id, function(err, contractor) {
    if(err) return next(err);
    if(!contractor) return next(new Error('Faild to load contractor: '+id))

    req.contractor = contractor;
    next();
  });
};


exports.read = function(req, res) {
  res.status(200).json(req.contractor);
}

exports.update = function(req, res) {
  var contractor = req.contractor;
  contractor.name = req.body.name;
  
  contractor.save(function(err) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(contractor);
    }
  });
};

exports.delete = function(req, res) {
  var contractor = req.contractor;
  contractor.remove(function(err) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(contractor);
    }
  })
}