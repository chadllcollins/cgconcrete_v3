const User = require('mongoose').model('User');
const passport = require('passport');

//Error-Handling
function getErrorMessage(err) {
  let message = '';

  if (err.code) {
    switch (err.code) {
      case 11000:
      case 11001:
        message = 'Username already exists';
        break;
      default:
        message = 'Something went wrong';
    }
  } else {
    for (var errName in err.errors) {
      if (err.errors[errName].message) message = err.errors[errName].message;
    }
  }

  return message;
};

//Encaspsulation of the authentication logic within controller
//Return the user for directly rendering the user in the Angular Application giving us access to the user
exports.signin = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if(err || !user) {
      res.status(400).send(info);
    } else {
      
      //Remove sensitive data before login
      user.password = undefined;
      user.salt = undefined;
      
      //req.login() exposed by passport module
      req.login(user, function(err) {
        if(err) {
          res.status(400).send(err);
        } else {
          res.json(user);
        }
      });
    }
  })(req, res, next);
};

exports.signup = function(req, res, next) {
  const user = new User(req.body);
  user.provider = 'local';
  user.save((err) => {
    if (err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      user.password = undefined;
      user.salt = undefined;
      
      //req.login() exposed by passport module
      req.login(user, function(err) {
        if(err) {
          res.status(400).send(err);
        } else {
          res.json(user);
        }
      });
    }
  });
};

exports.signout = function(req, res) {
  req.logout();
  res.redirect('/');
};

//isAuthenticated() exposed by the passport module
exports.requiresLogin = function(req, res, next) {
  if (!req.isAuthenticated()) {
    return res.status(401).send({
      message: 'User is not logged in'
    });
  }
  
  next();
};