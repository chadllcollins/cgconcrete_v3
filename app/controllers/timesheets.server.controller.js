const mongoose = require('mongoose');
const Timesheet = mongoose.model('Timesheet');

function getErrorMessage (err) {
  if (err.errors) {
    for (let errName in err.errors) {
      if (err.errors[errName].message) return err.errors[errName].message;
    }
  } else if(err.message) {
    return err.message;
  } else {
    return 'Unknown server error';
  }
};

exports.create = function(req, res) {
  const timesheet = new Timesheet(req.body);
  
  timesheet.save((err) => {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(timesheet)
    } 
  });
}

exports.list = function(req, res) {
  var start = req.query.start ? parseInt(req.query.start) : 0;
  var count = req.query.pageSize ? parseInt(req.query.pageSize) : 3;
  var filters = req.body.filter ? req.body.filter: {};
  var orderBy = req.body.orderBy ? req.body.orderBy : { submitTime: 1}
  Timesheet.find(filters)
    .sort(orderBy)
    .skip(start)
    .limit(count).exec((err, timesheets) => {
    if(err) {
      return res.status(200).json({
        message: getErrorMessage(err)
      });
    } else {
      res.json(timesheets);
    }
  })
}

exports.timesheetById = function(req, res, next, id) {
  Timesheet.findById(id, function(err, timesheet) {
    if(err) return next(err);
    if(!timesheet) return next(new Error('Failed to load timesheet: '+id));
    
    req.timesheet = timesheet
    next();
  });
}

exports.read = function(req, res) {
  res.json(req.timesheet);
}

exports.update = function(req, res) {
  var timesheet = req.timesheet;
  timesheet.lead = req.body.lead;
  timesheet.contractor = req.body.contractor;
  timesheet.project = req.body.project;
  timesheet.truckNumber = req.body.truckNumber;
  timesheet.tasked = req.body.tasked;
  timesheet.staffed = req.body.staffed;
  timesheet.endTime = req.body.endTime;
  timesheet.startTime = req.body.startTime;
  timesheet.notes = req.body.notes ? req.body.notes : '';
  timesheet.weatherInfo = req.body.weatherInfo ? req.body.weatherInfo : '';
  timesheet.yardsOfConcrete = req.body.yardsOfConcrete;
  
  timesheet.save((err) => {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(timesheet);
    }
  });
}

exports.delete = function(req, res) {
  var timesheet = req.timesheet;
  timesheet.remove((err) => {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(timesheet);
    }
  })
  
}