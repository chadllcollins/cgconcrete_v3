const mongoose = require('mongoose');
const Contractor = mongoose.model('Contractor');
const Project = mongoose.model('Project');
const Task = mongoose.model('Task');

function getErrorMessage (err) {
  if (err.errors) {
    for (let errName in err.errors) {
      if (err.errors[errName].message) return err.errors[errName].message;
    }
  } else {
    return 'Unknown server error';
  }
};

exports.create = function(req, res) {
  contractor = req.contractor;
  
  var project = new Project(req.body);
  project.contractor = contractor;
  
  project.save(function(err) {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      })
    } else {
      res.status(200).json(project);
    }
  })
}

exports.list = function(req, res) {
  Project.find({contractor: req.contractor}).sort('name').populate('contractor tasks').exec(function(err, projects) {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(projects);
    }
  });
};

exports.projectById = function(req, res, next, id) {
  Project.findById(id).populate('contractor tasks').exec(function(err, project) {
    if(err) return next(err);
    if(!project) return next(new Error('Failed to load project: '+id));
    
    req.project = project;
    next();
  });
}

exports.read = function(req, res) {
  res.json(req.project);
}

//We'll need to retrieve the Tasks assigned and verify they exist
exports.setTasks = function(req, res, next) {
  req.tasks = [];
  var objTasks = {}; //Using Object as a HashMap to remove duplocates
  var arrTasks; //Temp array for Task IDs
  //Reduce duplicate tasks
  //Use object as a hash function
  req.body.taskIds.forEach(function(taskId) {
    objTasks[taskId] = 'Placeholder';
  }); 
  arrTasks = Object.keys(objTasks); 
  arrTasks = arrTasks.map((str) => {return new mongoose.Types.ObjectId(str)}); //Convert strings to Mongoose IDs
                         
  Task.find({ '_id': { $in: arrTasks}}, function(err, tasks) {
    if(err) return next(err);
    if(!tasks) return next(new Error('Failed to load tasks for project: ' + req.project._id));

    req.tasks = tasks;
    next();
  });  
}

exports.update = function(req, res) {
  var project = req.project;
  project.name = req.body.name;
  project.tasks = req.tasks;
  
  project.save(function(err) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(project);
    }
  })
}

exports.delete = function(req, res) {
  var project = req.project;
  project.remove(function(err) {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(project);
    }
  })
}