const mongoose = require('mongoose');
const Employee = mongoose.model('Employee');

function getErrorMessage (err) {
  if (err.errors) {
    for (let errName in err.errors) {
      if (err.errors[errName].message) return err.errors[errName].message;
    }
  } else {
    return 'Unknown server error';
  }
}

exports.create = function(req, res) {
  const employee = new Employee(req.body);
  employee.save((err) => {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      return res.status(200).json(employee);
    }
  }); 
}

exports.list = function(req, res) {
  Employee.find({}).sort({firstName: 1, lastName: 1}).exec(function(err, employess) {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      })
    } else {
      res.json(employess);
    }
  });
};

exports.employeeById = function(req, res, next, id) {
  Employee.findById(id, (err, employee) => {
    if(err) return next(err);
    if(!employee) return next(new Error('Failed to load employee: '+id));
    
    req.employee = employee;
    next();
  });
};

exports.read = function(req, res) {
  res.status(200).json(req.employee);
}

exports.update = function(req, res) {
  var employee = req.employee;
  employee.firstName = req.body.firstName;
  employee.lastName = req.body.lastName;
  employee.leadRole = req.body.isLead;
  employee.save((err) => {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      });
    } else {
      res.status(200).json(employee);
    }
  });
}

exports.delete = function(req, res) {
  var employee = req.employee;
  employee.remove((err) => {
    if(err) {
      return res.status(400).json({
        message: getErrorMessage(err)
      })
    } else {
      res.status(200).json(employee);
    }
  })
}