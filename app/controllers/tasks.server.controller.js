const mongoose = require('mongoose');
const Task = mongoose.model('Task');

function getErrorMessage (err) {
  if (err.errors) {
    for (let errName in err.errors) {
      if (err.errors[errName].message) return err.errors[errName].message;
    }
  } else {
    return 'Unknown server error';
  }
};

exports.create = function(req, res) {
  const task = new Task(req.body);
  
  task.save((err) => {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(task);
    }
  });
}

exports.list = function(req, res) {
  Task.find().sort('name').exec(function(err, tasks) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(tasks);
    }
  })
}

//Expose the task for read/update/delete a particle task
exports.taskById = function(req, res, next, id) {
  Task.findById(id, (err, task) => {
    if(err) return next(err);
    if(!task) return next(new Error('Failed to load task: '+id));

    req.task = task;
    next();
  })
}

exports.read = function(req, res) {
  res.status(200).json(req.task);
}

exports.update = function(req, res) {
  var task = req.task;
  task.name = req.body.name;
  
  task.save(function(err) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(task);
    }
  });
}

exports.delete = function(req, res) {
  var task = req.task;
  task.remove(function(err) {
    if(err) {
      return res.status(400).send({
        message: getErrorMessage(err)
      });
    } else {
      res.json(task);
    }
  })
}