const contractors = require('../../app/controllers/contractors.server.controller');
const projects = require('../../app/controllers/projects.server.controller');

module.exports = function(app) {
  app.route('/api/contractors/:contractorId/projects')
    .get(projects.list)
    .post(projects.create);
  
  //Regex Next Time - Duplicate Information
  app.route('/api/contractors/:contractorId/projects/:projectId')
    .get(projects.read)
    .put(projects.setTasks, projects.update)
    .delete(projects.delete)
  app.route('/api/projects/:projectId')
    .get(projects.read)
    .put(projects.setTasks, projects.update)
    .delete(projects.delete)
  
  app.param('contractorId', contractors.contractorById);
  app.param('projectId', projects.projectById);
}