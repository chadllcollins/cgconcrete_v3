//API routes
const tasks = require('../../app/controllers/tasks.server.controller');

module.exports = function(app) {
  app.route('/api/tasks')
    .get(tasks.list)
    .post(tasks.create)
  app.route('/api/tasks/:taskId')
    .get(tasks.read)
    .put(tasks.update)
    .delete(tasks.delete)
  
  app.param('taskId', tasks.taskById);
}