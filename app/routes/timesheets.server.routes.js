const timesheets = require('../../app/controllers/timesheets.server.controller');

module.exports = function(app) {
  app.route('/api/timesheets')
    .get(timesheets.list)
    .post(timesheets.create);
  
  app.route('/api/timesheets/:timesheetId')
    .get(timesheets.read)
    .put(timesheets.update)
    .delete(timesheets.delete);
  
  app.param('timesheetId', timesheets.timesheetById);
}