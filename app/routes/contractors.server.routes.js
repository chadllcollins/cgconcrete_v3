const contractors = require('../../app/controllers/contractors.server.controller');

module.exports = function(app) {
  app.route('/api/contractors')
    .get(contractors.list)
    .post(contractors.create);
  
  app.route('/api/contractors/:contractorId')
    .get(contractors.read)
    .put(contractors.update)
    .delete(contractors.delete);
  
  app.param('contractorId', contractors.contractorById);
}
                            