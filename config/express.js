//Express is our web server responsible for serving up content from the backend
const config = require('./config');
const express = require('express');
const morgan = require('morgan');
const compress = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');

module.exports = function() {
  const app = express();
  
  if(process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
  } else if (process.env.NODE_ENV === 'production') {
    app.use(compress());
  }
  
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  
  /*app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: config.sessionSecret
  }));*/
          
  app.use(bodyParser.json());
  app.use(methodOverride());
  
  app.set('views', './app/views'); //Express application views controller
  app.set('view engine', 'ejs'); // Template Engine 
  
  app.use(flash());
  
  //app.use('/', express.static(path.resolve('./public')));
  //lib static route redirects to our node_modules - because client can't access node_module folder
  //app.use('/lib', express.static(path.resolve('./node_modules')));
  
  //app.use(express.static('./public'));
  
  require('../app/routes/tasks.server.routes.js')(app);
  require('../app/routes/employees.server.routes.js')(app);
  require('../app/routes/contractors.server.routes.js')(app);
  require('../app/routes/projects.server.routes.js')(app);
  require('../app/routes/timesheets.server.routes.js')(app);
  
  return app;
}