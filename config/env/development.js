//Environment Based configuration for the Dev environment
module.exports = {
  sessionSecret: 'devSpaceSessionSecret',
  db: 'mongodb://localhost/cgconcrete'
}