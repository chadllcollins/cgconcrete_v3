// MongoDB connection via Mongoose configuration
// Mongoose is responsible for our Model interacting with MongoDB
const config = require('./config');
const mongoose = require('mongoose');

module.exports = function() {
  //Only one db connection to mongoDB required
  const db = mongoose.connect(config.db);
  
  //Models
  require('../app/models/task.server.model.js');
  require('../app/models/project.server.model.js');
  require('../app/models/contractor.server.model.js');
  require('../app/models/employee.server.model.js');
 require('../app/models/timesheet.server.model.js');
  //require('../app/models/user.server.model.js');
  
  //Expose Database connection
  return db;
}